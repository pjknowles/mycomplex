#ifndef MYCOMPLEX_H
#define MYCOMPLEX_H
#include <cmath>

template <class T> class MyComplex
{
public:
    MyComplex() {
        re=im=T();
    }
    MyComplex(T const & r, T const & i){
        re=r;
        im=i;
    }

    T real() const { return re; }
    T real() { return re; }

    T imag() const { return im; }
    T imag() { return im; }

    T norm() const { return re * re + im * im; }
    T norm() { return re * re + im * im; }
    T abs() const { return std::sqrt(this->norm()); } // not so clever if not floating point scalar
    T abs() { return std::sqrt(this->norm()); }


MyComplex<T> & operator*=( const MyComplex<T> &z) {
    *this = MyComplex<T>(re*z.re - im*z.im, re*z.im + im*z.re);
    return *this;
}

MyComplex<T> operator*( const MyComplex<T> &z) {
    return MyComplex(*this) *= z;
}

MyComplex<T> & operator+=( const MyComplex<T> &z) {
    re += z.real();
    im += z.imag();
    return *this;
}

MyComplex<T> operator+( const MyComplex<T> &z) {
    return MyComplex(*this) += z;
}

MyComplex<T> & operator-=( const MyComplex<T> &z) {
    re -= z.real();
    im -= z.imag();
    return *this;
}

MyComplex<T> operator-( const MyComplex<T> &z) {
    return MyComplex(*this) -= z;
}

MyComplex<T> & operator/=( const MyComplex<T> &z) {
    double denom = z.norm();
    *this = MyComplex((re*z.re + im*z.im)/denom, (-re*z.im + im*z.re)/denom);
    return *this;
}

MyComplex<T> operator/( const MyComplex<T> &z) {
    return MyComplex(*this) /= z;
}

private:
    T re;
    T im;
};



#endif // MYCOMPLEX_H
