#include <iostream>
#include "MyComplex.h"

using namespace std;

int main()
{
    {
        MyComplex<double> z1(5,12);
        MyComplex<double> z2(5,-12);
        MyComplex<double> z3 = z1 * z2;
        cout << z3.real() <<", "<<z3.imag() <<", norm=" << z1.norm() << std::endl;
    }

    {
        MyComplex<int> z1(5,12);
        MyComplex<int> z2(5,-12);
        MyComplex<int> z3 = z1 * z2;
        cout << z3.real() <<", "<<z3.imag() <<", norm=" << z1.norm() << std::endl;
    }
    return 0;
}

